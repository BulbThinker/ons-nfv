package nfvs.utils.Dijkstra;

import nfvs.EONLinkE;
import nfvs.ILinkE;
import nfvs.Request;
import nfvs.disaster.DisasterZone;
import ons.*;
import ons.util.WeightedGraph;

import java.util.*;

public class DijkstraDisaster2 extends DijkstraExtra {

    private int[] DijkstraCore2(WeightedGraph G, int s, PhysicalTopology pt, int r, int cos) {
        final double[]  dist    = new double[G.size()];  // shortest known distance from "s"
        final int[]     pred    = new int[G.size()];     // preceding node in path
        final boolean[] visited = new boolean[G.size()]; // all false initially

        for (int i = 0; i < dist.length; i++) {
            pred[i] = -1;
            dist[i] = Double.POSITIVE_INFINITY;
            visited[i] = false;
        }
        dist[s] = 0;

        for (int i = 0; i < dist.length; i++) {
            final int next = DijkstraExtra.minVertex(dist, visited);
            if (next >= 0) {
                visited[next] = true;
                // The shortest path to next is dist[next] and via pred[next].
                final int[] n = G.neighbors(next);
                for (int j = 0; j < n.length; j++) {
                    final int v = n[j];
                    final double d;
                    final Link link = pt.getLink(next, v);
                    d = dist[next] + this.getDijkstraWeight2(link,r,cos);
                    if (dist[v] > d) {
                        dist[v] = d;
                        pred[v] = next;
                    }
                }
            }
            else {
                break;
            }
        }
        return pred;
    }

    @Override
    public int[] getKShortestPath(WeightedGraph G, int src, int dst, int k, PhysicalTopology pt, int r) {
        float c = 0;
        int x;
        int[] sp;
        ArrayList<Integer> path = new ArrayList<>();
        final int[] pred = this.DijkstraCore2(G, src, pt, r, k);

        x = dst;

        while (x != src) {
            path.add(0, x);
            x = pred[x];
            // No path
            if (x == -1) {
                return new int[0];
            }
        }
        path.add(0, src);
        sp = new int[path.size()];
        for (int i = 0; i < path.size(); i++) {
            sp[i] = path.get(i);
        }
        return sp;
    }

    public double getDijkstraWeight(WeightedGraph G, Link link, int r) {
        return getDijkstraWeight2(link,r,0);
    }
    
    public double getDijkstraWeight2(Link link, int r, int cos) {
        double ldp = (DisasterZone.getLinkDisasterProbability(link));
        double lsc = (((ILinkE)link).getSetupCost());
        double abr;
        if(link instanceof EONLink) {
            abr = ((EONLinkE)link).getContinuosAvailableBR();
        }
        else {
            abr = ((ILinkE)link).getAvailableBR();
        }
        if(link.getWeight() == Double.POSITIVE_INFINITY) {
            return Double.POSITIVE_INFINITY;
        }
        //
        if(link instanceof WDMLink) {
            if(((WDMLink)link).hasBWAvailable(r).length <= 0) {
                return Double.POSITIVE_INFINITY;
            }
        }
        //EON
        else if(r > abr) {
            return Double.POSITIVE_INFINITY;
        }
        //
        if(abr == 0) {
            return Double.POSITIVE_INFINITY;
        }
        //v2 ((lsc/100)/(4-cos) + (r/abr)*(4-cos))*(1 + ldp*(4-cos))
        //v3 ((lsc/100)/(4-cos) + (r/abr)*(4-cos))*(1 + ldp*(cos + 1))
        //v4 ((lsc/100)/(4-cos) + (r/abr))*(1 + ldp)
        return ((lsc/70.0)+ (r/abr)*(4 - cos) + ldp)*(1 + ldp + (4 - cos));
    }
}
