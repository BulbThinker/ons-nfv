package nfvs.utils.Dijkstra;

import ons.EONLink;
import ons.EONPhysicalTopology;
import ons.Link;
import ons.WDMLink;
import ons.util.WeightedGraph;

public class ConstrainedDijiktra extends DijkstraExtra{

	public  double getDijkstraWeight(WeightedGraph G, Link link, int r) {
		if(link instanceof WDMLink) {     	
        	if(((WDMLink)link).hasBWAvailable(r).length <= 0) {
        		return Double.POSITIVE_INFINITY;
        	}
        	else {
        		return (G.getWeight(link.getSource(), link.getDestination()));
        	}
        }
        else {
        	//EON
        	if(r > (((EONLink)link).getAvaiableSlots() * EONPhysicalTopology.getSlotSize())) {
        		return Double.POSITIVE_INFINITY;
        	}
        	else {
        		return (G.getWeight(link.getSource(), link.getDestination()));
        	}
        }
	}
	
}
