package nfvs.utils.Dijkstra;


import java.util.*;

import nfvs.utils.WPath;
import ons.Link;
import ons.PhysicalTopology;
import ons.util.WeightedGraph;

public class DijkstraExtra{

	protected static int minVertex(double[] dist, boolean[] v) {
        double x = Double.MAX_VALUE;
        int y = -1;   // graph not connected, or no unvisited vertices
        for (int i = 0; i < dist.length; i++) {
            if (!v[i] && dist[i] < x) {
                y = i;
                x = dist[i];
            }
        }
        return y;
    }

	protected int[] DijkstraCore(WeightedGraph G, int s, PhysicalTopology pt, int r) {
		final double[]  dist    = new double[G.size()];  // shortest known distance from "s"
        final int[]     pred    = new int[G.size()];     // preceding node in path
        final boolean[] visited = new boolean[G.size()]; // all false initially
        
        for (int i = 0; i < dist.length; i++) {
            pred[i] = -1;
            dist[i] = Double.POSITIVE_INFINITY;
            visited[i] = false;
        }
        dist[s] = 0;
        
        for (int i = 0; i < dist.length; i++) {
            final int next = DijkstraExtra.minVertex(dist, visited);
            
            if (next >= 0) {
                visited[next] = true;
                // The shortest path to next is dist[next] and via pred[next].
                final int[] n = G.neighbors(next);
                for (int j = 0; j < n.length; j++) {
                    final int v = n[j];
                    final double d;
                    final Link link = pt.getLink(next, v);
                    d = dist[next] + this.getDijkstraWeight(G,link,r);
                    if (dist[v] > d) {
            			dist[v] = d;
                        pred[v] = next;
                    }
                }
            }
            else {
            	break;
            }
        }
        return pred; 
	}

	public double[] getAllHops(WeightedGraph G, int src) {
        final double[]  dist    = new double[G.size()];  // shortest known distance from "s"
        final int[]     pred    = new int[G.size()];     // preceding node in path
        final boolean[] visited = new boolean[G.size()]; // all false initially

        for (int i = 0; i < dist.length; i++) {
            pred[i] = -1;
            dist[i] =  Double.POSITIVE_INFINITY;
            visited[i] = false;
        }
        dist[src] = 0;

        for (int i = 0; i < dist.length; i++) {
            final int next = DijkstraExtra.minVertex(dist, visited);

            if (next >= 0) {
                visited[next] = true;
                // The shortest path to next is dist[next] and via pred[next].
                final int[] n = G.neighbors(next);
                for (int j = 0; j < n.length; j++) {
                    int v = n[j];
                    double d;
                    d = dist[next] + ((G.getWeight(next, v) != Double.POSITIVE_INFINITY)?1.0:Double.POSITIVE_INFINITY);
                    if (dist[v] > d) {
                        dist[v] = d;
                        pred[v] = next;
                    }
                }
            }
            else {
                break;
            }
        }
        return dist;
    }

	public double[] getAllDistances(WeightedGraph G, int src) {
		final double[]  dist    = new double[G.size()];  // shortest known distance from "s"
        final int[]     pred    = new int[G.size()];     // preceding node in path
        final boolean[] visited = new boolean[G.size()]; // all false initially
        
        for (int i = 0; i < dist.length; i++) {
            pred[i] = -1;
            dist[i] = Double.POSITIVE_INFINITY;
            visited[i] = false;
        }
        dist[src] = 0;
        
        for (int i = 0; i < dist.length; i++) {
            final int next = DijkstraExtra.minVertex(dist, visited);
            
            if (next >= 0) {
                visited[next] = true;
                // The shortest path to next is dist[next] and via pred[next].
                final int[] n = G.neighbors(next);
                for (int j = 0; j < n.length; j++) {
                    final int v = n[j];
                    final double d;
                    d = dist[next] + G.getWeight(next, v);
                    if (dist[v] > d) {
            			dist[v] = d;
                        pred[v] = next;
                    }
                }
            }
            else {
            	break;
            }
        }
        return dist;
	}

	public double[] getAllMinDistances(WeightedGraph G, int src, int dst) {
        double distS[]  = this.getAllDistances(G, src);
        double distD[] =  this.getAllDistances(G, dst);
        double dist[] = new double[distS.length];
        for(int i = 0; i < dist.length; ++i) {
            dist[i] = (distS[i] < distD[i])?distS[i]:distD[i];
        }
        return dist;
    }

	public int[] getShortestPath(WeightedGraph G, int src, int dst, PhysicalTopology pt, int r) {
        int x;
        int[] sp;
        ArrayList<Integer> path = new ArrayList<>();
        final int[] pred = this.DijkstraCore(G, src, pt, r);

        x = dst;

        while (x != src) {
            path.add(0, x);
            x = pred[x];
            // No path
            if (x == -1) {
                return new int[0];
            }
        }
        path.add(0, src);
        sp = new int[path.size()];
        for (int i = 0; i < path.size(); i++) {
            sp[i] = path.get(i);
        }
        return sp;
    }

    public int[] getKShortestPath(WeightedGraph G, int src, int dst, int k, PhysicalTopology pt, int r) {
        int cos = k;
	    k += 1;
	    PriorityQueue<WPath> B = new PriorityQueue<>(100, new WPath.PathSort());
        List<WPath> P = new ArrayList<>(k);
        int[] count = new int[G.size()];
        WPath aux;
        //count[u] = 0, for all u in V
        for(int i = 0; i < count.length; ++i) {
            count[i] = 0;
        }
        //insert path P[src] = {s} into B with cost 0
        aux = new WPath(1,0.0);
        aux.add(src);
        B.add(aux);
        //while B is not empty and count[dst] < k
        while((!B.isEmpty())&&(count[dst] < k)) {
            //let Pu be the shortest cost path in B with cost C
            //B = B − {Pu }, count[u] = count[u] + 1
            WPath Pu = B.poll();
            double C = Pu.weight;
            int    u = Pu.get(Pu.size() - 1);
            count[u] += 1;
            if(u == dst) {
                // if u = t then P = P U {Pu}
                P.add(Pu);
            }
            if(count[u] <= k) {
                //if count[u] ≤ K then
                //for each vertex v adjacent to u:
                for(int v : G.neighbors(u)) {
                    //let Pv be a new path with cost C + w(u, v) formed by concatenating edge (u, v) to path Pu
                    Link l = pt.getLink(u,v);
                    WPath Pv = new WPath(Pu.size()+1,  C + getDijkstraWeight(G,l,r));
                    Pv.addAll(Pu);
                    Pv.add(v);
                    B.add(Pv);
                }
            }
        }
	    P.sort(new WPath.PathSort());
        if(P.isEmpty()) {
            return new int[0];
        }
        double auxw = Double.POSITIVE_INFINITY;
        for(int i = cos;(i>=0)&(auxw==Double.POSITIVE_INFINITY);i--){
            aux  = P.get(i);
            auxw = aux.weight;
        }
        return aux.toIntArray();
    }

    public int[] getOrderRestrictKShortestPath(WeightedGraph G, int src, int dst, int k, PhysicalTopology pt, int r, int[] nodes) {
        int cos = k;
        k += 1;
        PriorityQueue<WPath> B = new PriorityQueue<>(100, new WPath.PathSort());
        List<WPath> P = new ArrayList<>(k);
        int[] count = new int[G.size()];
        WPath aux;
        //count[u] = 0, for all u in V
        for(int i = 0; i < count.length; ++i) {
            count[i] = 0;
        }
        //insert path P[src] = {s} into B with cost 0
        aux = new WPath(1,0.0);
        aux.add(src);
        B.add(aux);
        //while B is not empty and count[dst] < k
        while((!B.isEmpty())&&(count[dst] < k)) {
            //let Pu be the shortest cost path in B with cost C
            //B = B − {Pu }, count[u] = count[u] + 1
            WPath Pu = B.poll();
            double C = Pu.weight;
            int    u = Pu.get(Pu.size() - 1);
            //
                //Restrict
                boolean restrict = false;
                boolean invalid  = false;
                for(int i = 0; i < nodes.length; ++i) {
                    if (Pu.contains(nodes[i])) {
                        if((i!=0)&&(!restrict)) {
                            invalid = true;
                            break;
                        }
                        restrict = true;
                    }
                }
                if(invalid) continue;
            //
            if(u == dst) {
                // if u = t then P = P U {Pu}
                for (int node : nodes) {
                    if (!Pu.contains(node)) {
                        invalid = true;
                    }
                }
                if(invalid)
                    continue;
                else
                    P.add(Pu);
            }
            count[u] += 1;
            if(count[u] <= k) {
                //if count[u] ≤ K then
                //for each vertex v adjacent to u:
                for(int v : G.neighbors(u)) {
                    //let Pv be a new path with cost C + w(u, v) formed by concatenating edge (u, v) to path Pu
                    Link l = pt.getLink(u,v);
                    WPath Pv = new WPath(Pu.size()+1,  C + getDijkstraWeight(G,l,r));
                    Pv.addAll(Pu);
                    Pv.add(v);
                    B.add(Pv);
                }
            }
        }
        P.sort(new WPath.PathSort());
        if(P.isEmpty()) {
            return new int[0];
        }
        double auxw = Double.POSITIVE_INFINITY;
        for(int i = cos;(i>=0)&(auxw==Double.POSITIVE_INFINITY);i--){
            aux  = P.get(i);
            auxw = aux.weight;
        }
        return aux.toIntArray();
    }

	protected double getDijkstraWeight(WeightedGraph G, Link link, int r) {
		return (double)(G.getWeight(link.getSource(), link.getDestination()));
	}

}
