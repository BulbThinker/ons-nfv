package nfvs.utils;

import java.util.Random;

public class RandomE extends Random {

    public RandomE(int seed) {
        super(seed);
    }

    public  RandomE(int seed, int second_seed) {
        super(seed + second_seed);
    }

    public void setSeed(int seed, int second_seed) {
        super.setSeed(seed + second_seed);
    }

    public float getFloatInterval(float min, float max) {
        return ((max - min)*nextFloat())+min;
    }

    public double nextExponential(double lambda) {
        return this.nextExponentialB(1/lambda);
    }

    public double nextExponentialB(double beta) {
        return (-beta *Math.log(1-this.nextDouble()));
    }

}
