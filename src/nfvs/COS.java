package nfvs;


import nfvs.utils.RandomE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class COS {
    private static Map<Integer, COS> cos_map;
    public  static RandomE rand;

    private int id;
    private float br_min;
    private float br_max;
    private float t_min;
    private float t_max;

    static {
        cos_map = new HashMap<>();
        rand    = new RandomE(0, 1);
    }

    private COS(int id, float br_min, float br_max, float t_min, float t_max) {
        this.id = id;
        this.br_min = br_min;
        this.br_max = br_max;
        this.t_min = t_min;
        this.t_max = t_max;
    }

    public static COS registerCOS(int id, float br_min, float br_max, float t_min, float t_max) {
        if(cos_map.containsKey(id)){
            return cos_map.get(id);
        }
        return cos_map.put(id, new COS(id, br_min, br_max, t_min, t_max));
    }

    public static int getNumCos() {
        return cos_map.size();
    }

    public static COS getCOS(int id) {
        if(cos_map.containsKey(id)){
            return cos_map.get(id);
        }
        return null;
    }

    public int getID() {
        return id;
    }

    public float getBRTolerance() {
        return rand.getFloatInterval(br_min, br_max);
    }

    public int getBRTolerance(int br) {
        return (int)Math.abs(br * getBRTolerance());
    }

    public float getTimeTolerance() {
        return rand.getFloatInterval(t_min, t_max);
    }

    public float getTimeTolerance(float time) {
        return (int)Math.abs(time * getTimeTolerance());
    }

    public float[] getBRToleranceLim() {
        float[] l = {br_min, br_max};
        return l;
    }

    public float[] getTimeToleranceLim() {
        float[] l = {t_min, t_max};
        return l;
    }

}
