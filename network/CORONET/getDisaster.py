florida = list(set([20, 54, 110, 136, 138, 144, 166, 196]))
florida.sort()
#print("Florida")
#print("{}\n".format(florida))
#
lousiana = list(set([32, 34, 44, 144]))
lousiana.sort()
#print("Lousiana")
#print("{}\n".format(lousiana))
#
new_york = list(set([4, 6, 50, 52, 106, 124, 146, 148, 150, 184, 190]))
new_york.sort()
#print("New York")
#print("{}\n".format(new_york))
#
north_carolina = list(set([18, 56, 58, 100, 102, 104, 154]))
north_carolina.sort()
#print("North Carolina")
#print("{}\n".format(north_carolina))
#
texas = list(set([0, 2, 8, 12, 22, 24, 32, 78, 80, 82, 90, 92]))
texas.sort()
#print("Texas")
#print("{}\n".format(texas))
#
p = {
	0:0.33,
	2:0.33,
	4:0.08,
	6:0.08,
	8:0.33,
	12:0.33,
	18:0.28,
	20:0.51,
	22:0.33,
	24:0.33,
	32:0.53,
	34:0.30,
	44:0.30,
	50:0.08,
	52:0.08,
	54:0.51,
	56:0.28,
	58:0.28,
	78:0.33,
	80:0.33,
	82:0.33,
	90:0.33,
	92:0.33,
	100:0.28,
	102:0.28,
	104:0.28,
	106:0.08,
	110:0.51,
	124:0.08,
	136:0.51,
	138:0.51,
	144:0.66,
	146:0.08,
	148:0.08,
	150:0.08,
	154:0.28,
	166:0.51,
	184:0.08,
	190:0.08,
	196:0.51,
}

zones = [florida, texas, lousiana, north_carolina, new_york]

#print('\n\n')

print('<disaster MTBF="1000" MRT="10" selected="-1" number="1">')

for zid in range(len(zones)):
	print('    <zone id="{}">'.format(zid))
	for lid in zones[zid]:
		print('        <link id="{0}" probability="{1}"/>'.format(lid, p[lid]))
	print('    </zone>')
print('</disaster>')