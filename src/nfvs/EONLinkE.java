package nfvs;

import ons.EONLink;
import ons.EONPhysicalTopology;

import java.util.LinkedHashSet;
import java.util.Set;

public class EONLinkE extends EONLink implements ILinkE{
	
	private int setup_cost;
	private Set<Request> activeRequests;
	
	public EONLinkE(int id, int src, int dst, double delay, double weight, int numSlots, int guardband, int cost_min, int cost_max) {
		super(id, src, dst, delay, weight, numSlots, guardband);
		this.setup_cost = NFVRandom.nextIntInterval(cost_min, cost_max);
		this.activeRequests = new LinkedHashSet<>();
	}

	public EONLinkE(int id, int src, int dst, double delay, double weight, int numSlots, int guardband, int setup_cost) {
		super(id, src, dst, delay, weight, numSlots, guardband);
		this.setup_cost = setup_cost;
		this.activeRequests = new LinkedHashSet<>();
	}
	
	public int getSetupCost() {
		return this.setup_cost;
	}

	@Override
	public void addRequest(Request r) {
		this.activeRequests.add(r);
	}

	@Override
	public void removeRequest(Request r) {
		this.activeRequests.remove(r);
	}

	@Override
	public void removeRequests(Request[] r) {
		for(Request rq : r) {
			removeRequest(rq);
		}
	}

	@Override
	public double getAvailableBR() {
		return (this.getAvaiableSlots() * EONPhysicalTopology.getSlotSize());
	}

	@Override
	public Set<Request> getRequests() {
		return this.activeRequests;
	}

	public double getContinuosAvailableBR() {
		long maxAvailableSlots = 0;
		long availableSlots = 0;
		for(int i = 0; i < slots.length; ++i) {
			if(slots[i] == 0) {
				availableSlots++;
			}
			else {
				if(availableSlots > maxAvailableSlots) {
					maxAvailableSlots = availableSlots;
				}
				availableSlots = 0;
			}
		}
		if(availableSlots > maxAvailableSlots) {
			maxAvailableSlots = availableSlots;
		}
		return (maxAvailableSlots * EONPhysicalTopology.getSlotSize());
	}
}
