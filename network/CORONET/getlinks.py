import math
NODES_FP = "nodes.txt"
LINKS_FP = "links.txt"

with open(NODES_FP, "r") as f:
	nodes = f.read().split()

nodes_map = {nodes[i]:i for i in range(len(nodes))}

del nodes

with open(LINKS_FP, "r") as f:
	links = f.read().split()

lid = 0
for i in range(len(links)//3):
	origem    = nodes_map[links[3*i]]
	destino   = nodes_map[links[(3*i)+1]]
	distancia = float(links[(3*i)+2])
	
	print('<link id="{0}"  source="{1}"  destination="{2}"  delay="{3:.2f}" weight="{4}"  cost_min="20" cost_max="50"/>'.format(lid,origem,destino,distancia/200,math.floor(distancia)))
	print('<link id="{0}"  source="{1}"  destination="{2}"  delay="{3:.2f}" weight="{4}"  cost_min="20" cost_max="50"/>'.format(lid+1,destino,origem,distancia/200,math.floor(distancia)))
	lid += 2