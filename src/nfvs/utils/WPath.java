package nfvs.utils;

import java.util.ArrayList;
import java.util.Comparator;

public class WPath extends ArrayList<Integer> implements Comparable<WPath>{

    public double weight;

    public WPath(int initialCapacity, double weight) {
        super(initialCapacity);
        this.weight = weight;
    }

    public WPath(double weight) {
        super();
        this.weight = weight;
    }

    public WPath(int initialCapacity) {
        super(initialCapacity);
        this.weight = 0;
    }

    public WPath() {
        super();
        this.weight = 0;
    }

    @Override
    public int compareTo(WPath p) {
        if(this.weight < p.weight) {
            return -1;
        }
        else if (this.weight > p.weight){
            return 1;
        }
        return 0;
    }

    public int[] toIntArray() {
        int[] ret = new int[size()];
        for(int i = 0; i < ret.length; ++i) {
            ret[i] = get(i);
        }
        return ret;
    }

    public static class PathSort implements Comparator<WPath> {

        @Override
        public int compare(WPath o1, WPath o2) {
            if(o1.weight < o2.weight) {
                return -1;
            }
            else if (o1.weight > o2.weight){
                return 1;
            }
            return 0;
        }
    }

    public static class PathSortR implements Comparator<WPath> {
        @Override
        public int compare(WPath o1, WPath o2) {
            if(o1.weight < o2.weight) {
                return 1;
            }
            else if (o1.weight > o2.weight){
                return -1;
            }
            return 0;
        }
    }
}
