package nfvs;

import nfvs.events.DisasterEvent;
import nfvs.events.DisasterPredictionEvent;
import nfvs.utils.RandomE;
import ons.Event;

public class DisasterPredictor {

    public static Class dpClass;

    public static double accuracity;
    public static double sd;

    public  static RandomE rand;

    static {
        rand = new RandomE(0,0);
        accuracity = 0.0;
        sd = 0;
        dpClass = null;
    }

    public static Event predict(DisasterEvent event) {
        if(rand.nextDouble() < accuracity) {
            double time = event.getTime() - Math.abs(rand.nextGaussian() * sd);
            DisasterPredictionEvent pe = new DisasterPredictionEvent();
            pe.setEvent(event);
            pe.setTime(time);
            return pe;
        }
        return event;
    }

}
