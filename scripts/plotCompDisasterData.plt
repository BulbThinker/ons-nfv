#!/usr/bin/gnuplot

# TITLEBASE OUTPUT TITLE1 FILE1 TITLE2 FILE2

if (ARGC!=7) {
	print "Required Arguments: TITLEBASE OUTPUT TITLE1 FILE1 TITLE2 FILE2"
	exit
}

#Constants
OUTPATH=ARG2.'/'
FILE1=ARG4
FILE2=ARG6
TITLE1=ARG3
TITLE2=ARG5
TITLEBASE=ARG1

#Initial Configuration
set term png size 1024,768
set xlabel "Load (Erlangs)"

#Linear

#Histogram
set boxwidth 0.9 relative
set style data histogram
set style fill solid 1.0 border -1
set bars fullwidth
set style histogram errorbars gap 2 lw 2

#BBR
set title  TITLEBASE."\nBBR"
set ylabel "BBR (%)"
set output OUTPATH."BBR.png"
plot FILE1 using 22:23:xticlabels(1) title TITLE1, \
     FILE2 using 22:23 title TITLE2

#BR
set title  TITLEBASE."\nBR"
set ylabel "BR (%)"
set output OUTPATH."BR.png"
plot FILE1 using 24:25:xticlabels(1) title TITLE1, \
     FILE2 using 24:25 title TITLE2

#Blocked Requests (COS)
set title  TITLEBASE."\nBP (COS)"
set ylabel "BP (%)"
set output OUTPATH."BP_COS.png"
plot FILE1 using 2:3:xticlabels(1) title TITLE1."(Hard Real-Time)", \
	 FILE1 using 4:5 title TITLE1."(Soft Bandwidth)", \
	 FILE1 using 6:7 title TITLE1."(Soft Delay)", \
	 FILE1 using 8:9 title TITLE1."(Non Real-Time)", \
	 FILE2 using 2:3 title TITLE2."(Hard Real-Time)", \
	 FILE2 using 4:5 title TITLE2."(Soft Bandwidth)", \
	 FILE2 using 6:7 title TITLE2."(Soft Delay)", \
	 FILE2 using 8:9 title TITLE2."(Non Real-Time)"

#Cost
set title  TITLEBASE."\Costs"
set ylabel "Cost ($)"
set output OUTPATH."Cost.png"
plot FILE1 using 18:19:xticlabels(1) title TITLE1."(Blocked Cost)", \
	 FILE1 using 20:21 title TITLE1."(Disaster Cost)", \
	 FILE1 using ($18+$20):(sqrt((($19)**2)+(($21)**2))) title TITLE1."(TOTAL)" with lines, \
	 FILE2 using 18:19 title TITLE2."(Blocked Cost)", \
	 FILE2 using 20:21 title TITLE2."(Disaster Cost)", \
	 FILE2 using ($18+$20):(sqrt((($19)**2)+(($21)**2))) title TITLE2."(TOTAL)" with lines

#Blocked Requests
set title  TITLEBASE."\nBlocked Requests"
set ylabel "Blocked/Total Blocked (%)"
set output OUTPATH."Blocked_COS.png"
plot FILE1 using 26:27:xticlabels(1) title TITLE1." By DC", \
     FILE1 using 28:29 title TITLE1." By VNF" , \
	 FILE1 using 30:31 title TITLE1." By BW"  , \
	 FILE1 using 32:33 title TITLE1." By Path", \
	 FILE2 using 26:27 title TITLE2." By DC", \
     FILE2 using 28:29 title TITLE2." By VNF" , \
	 FILE2 using 30:31 title TITLE2." By BW"  , \
	 FILE2 using 32:33 title TITLE2." By Path"
exit

