package nfvs;

import nfvs.utils.RandomE;

public class NFVRandom {
	protected static RandomE rnd;
	
	static {
		NFVRandom.rnd = new RandomE(0,1);
	}

	public static void setSeed(int seed) {
		NFVRandom.rnd.setSeed(seed);
	}

	public static void setSeed(int seed, int second) {
		NFVRandom.rnd.setSeed(seed, second);
	}
	
	public static int nextInt(int bound) {
		return NFVRandom.rnd.nextInt(bound);
	}
	
	public static int nextInt() {
		return NFVRandom.rnd.nextInt();
	}
	
	public static int nextIntInterval(int min, int max) {
		return (NFVRandom.rnd.nextInt(max - min + 1) + min);
	}
	
}
