package nfvs;

import java.util.Arrays;
import java.util.List;

import ons.*;
import ons.util.WeightedGraph;

public class Request extends Flow {

	public enum Flag{
		DEGRADED(0x01),
		POSTPONED(0x02);

		public byte valor;
		Flag(int valor){
			this.valor = (byte)valor;
		}

	}


	private List<Integer> requested_functions;
	private VNF[] used_functions;
	private FlowDepartureEvent departureEvent;

	private int bwt;
	private float ttp;

	private byte flags;

	private double remain_duration;

	public Request(long id,int src, int dst, int bw, int duration, int cos, List<Integer> vnfs) {
		super(id,src,dst,bw,duration,cos);
		this.requested_functions = vnfs;
		this.used_functions = new VNF[vnfs.size()];
		for(int i = 0; i < this.used_functions.length; ++i) {
			this.used_functions[i] = null;
		}
		departureEvent = null;
		bwt = 0;
		ttp = 0;
		flags = 0x00;
		remain_duration = duration;
	}

	public double setRemainDuration(double rd) {
		this.remain_duration = rd;
		return rd;
	}

	public double getRemainDuration() {
		return this.remain_duration;
	}

	public void setFlag(Flag f) {
		flags = (byte)(flags|f.valor);
	}

	public boolean getFlag(Flag f) {
		return ((flags&f.valor)!=0);
	}

	/**
	*	Degrade Request BW (Don't modify PT)
	*   @param current_time Current Time of Simulation
    *   @return Remain Duration of Request
	*/
	public double degradeBW(double current_time, boolean EON) {

		if(!getFlag(Flag.DEGRADED)) {
			double rd = departureEvent.getTime() - current_time;
			//Set Rate
			double r  = this.getRate();
			int newRate;
			if(EON) {
				double aux =  Math.ceil((this.getRate() - bwt)/((double)EONPhysicalTopology.getSlotSize()));
				aux *= EONPhysicalTopology.getSlotSize();
				newRate = (int) aux;
				if(newRate > this.getRate()) {
					newRate = this.getRate();
				}
			}
			else {
				newRate = this.getRate() - bwt;
			}
			this.setRate(newRate);
			r /= this.getRate();
			this.setFlag(Flag.DEGRADED);
			rd *= r;
			setRemainDuration(rd);
			//
			departureEvent.setTime(current_time + remain_duration);
		}
		return getRemainDuration();
	}

	public void setRequestedFunctions(List<Integer> vnfs) {
		this.requested_functions = vnfs;
		this.used_functions = new VNF[vnfs.size()];
	}

	public void setBWTolerance(int bw) {
		bwt = bw;
	}

	public void setTimePercentualTolerance(float timep) {
		ttp = timep;
	}

	public double getTimePercentualTolerance() {
		return  ttp;
	}

	public FlowDepartureEvent setDepartureEvent(FlowDepartureEvent event) {
		return (departureEvent = event);
	}

	public FlowDepartureEvent getDepartureEvent(){
		return departureEvent;
	}

	public int[] getRequestedFunctions() {
		int[] rf = new int[this.requested_functions.size()];
		for(int i = 0; i < rf.length; ++i) {
			rf[i] = this.requested_functions.get(i);
		}
		return rf;
	}
	
	public VNF[] getUsedFunctions() {
		return Arrays.copyOf(this.used_functions, this.used_functions.length);
	}
	
	public void setUsedFunction(int index, VNF vnf) {
		this.used_functions[index] = vnf;
	}
	
	public synchronized void freeFunctions() {
		for (int i = 0; i < this.used_functions.length; ++i) {
			if(this.used_functions[i] != null) {
				this.used_functions[i].endRequest();
				this.used_functions[i].getDatacenter().tryFreeResources(this.used_functions[i]);
				this.used_functions[i] = null;
			}
		}
	}

}