#!/usr/bin/gnuplot
set print "-"
FILEPATH="NFVRWA_CSP_H_ERLANG/"
FILEBASE="simulation-NFVRWA_CSP_H_"
CONFIDENCE_LEVEL=1.96

print "#Requests Blocked Blocked_Err BR BR_Err BBR BBR_Err RoutingCost RoutingCost_Err UsedLinks UsedLinks_Err SetupCost SetupCost_Err SatisfiedFunctions SatisfiedFunctions_Err InstancedFunctions InstancedFunctions_Err BlockedByDC BlockedByDC_Err BlockedByVNF BlockedByVNF_Err BlockedByBW BlockedByBW_Err BlockedByPath BlockedByPath_Err"

do for [r in "5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100"] {
    out=r  
    file=FILEPATH.FILEBASE.r."_RESULT"
    do for [c=1:12:1] {
        stats file using c nooutput
        out=out.sprintf(" %f %f", STATS_mean, CONFIDENCE_LEVEL*STATS_mean_err)
    }
    print out
}

exit
