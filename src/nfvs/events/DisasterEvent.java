package nfvs.events;

import nfvs.ILinkE;
import nfvs.Request;
import nfvs.disaster.DisasterGenerator;
import nfvs.disaster.DisasterZone;
import ons.*;

import java.util.*;

public class DisasterEvent extends EventE {
	private DisasterZone zone;
	private Link[] affectedLinks;
	private RepairEvent revent;

	public  DisasterEvent(){
		zone = null;
		affectedLinks = null;
		revent = null;
	}

	public void setZone(DisasterZone zone) {
		this.zone = zone;
	}

	public void setLinks(Link[] links) {
		this.affectedLinks = links.clone();
	}

	public void linkRepairEvent(RepairEvent revent) {
		this.revent = revent;
	}

	public Link[] defineEvent() {
		if(affectedLinks == null) {
			this.affectedLinks = (zone.getRandomLinks());
		}
		return affectedLinks;
	}

	public Set<Request> execute() {
		//
		defineEvent();
		//
		Set<Request> requests = new HashSet<>();
		//if(!(zone.getStatus())){
			//return requests;
		//}
		//Get the affected requests and interrupt the links.
		for(Link l : affectedLinks) {
			l.setWeight(Double.POSITIVE_INFINITY);
			requests.addAll(((ILinkE)l).getRequests());
		}
		//Temporary Block Requests (Remove Departure Event)
		for (Request rq : requests) {
			EventE.eventScheduler.removeEvent(rq.getDepartureEvent());
		}
		//
		if(Simulator.verbose) {
			System.out.println("Disaster Event at zone " + zone.getId() + ", " + affectedLinks.length + " affected links, " + requests.size() + " Requests affected.");
		}
		zone.setAffected();
		//Create Recovery Event
		if(!requests.isEmpty()) {
			RecoveryEvent re = new RecoveryEvent();
			re.setTime(getTime());
			re.addRequests(requests);
			eventScheduler.addEvent(re);
		}
		//Create Repair Event
		revent = new RepairEvent();
		revent.setZone(this.zone);
		revent.setLinks(this.affectedLinks);
		revent.setTime(this.getTime() + (DisasterGenerator.rand.nextExponentialB(DisasterGenerator.MRT * affectedLinks.length)));
		eventScheduler.addEvent(revent);

		//Update Graph
		cp.getRA().simulationInterface(cp);

		return requests;
	}
}
