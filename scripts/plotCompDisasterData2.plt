#!/usr/bin/gnuplot

# TITLEBASE OUTPUT TITLE1 FILE1 TITLE2 FILE2

#Constants
OUTPATH="../GRAPH/RA vs RA-COS/"
FILE1="../DATA/RESULT_RA.data"
FILE2="../DATA/RESULT_RA_COS.data"
TITLE1="RA"
TITLE2="RA - COS"

TITLEBASE="EON RA - COS"

#Initial Configuration
set term png size 1024,768
set xlabel "Load (Erlangs)"

#Linear

#Histogram
set boxwidth 0.9 relative
set style data histogram
set style fill solid 1.0 border -1
set bars fullwidth
set style histogram errorbars gap 2 lw 2

#BBR
set title  TITLEBASE."\nBBR"
set ylabel "BBR (%)"
set output OUTPATH."BBR.png"
plot FILE1 using 22:23:xticlabels(1) title TITLE1, \
     FILE2 using 22:23 title TITLE2#, \
     #FILE3 using 22:23 title TITLE3

#BR
set title  TITLEBASE."\nBR"
set ylabel "BR (%)"
set output OUTPATH."BR.png"
plot FILE1 using 24:25:xticlabels(1) title TITLE1, \
     FILE2 using 24:25 title TITLE2#, \
     #FILE3 using 25:25 title TITLE3

#Blocked Requests (COS)
set title  TITLEBASE."\nBP (COS)"
set ylabel "BP (%)"
set output OUTPATH."BP_COS.png"
plot FILE1 using 2:3:xticlabels(1) title TITLE1."(Hard Real-Time)", \
	 FILE1 using 4:5 title TITLE1."(Soft Bandwidth)", \
	 FILE1 using 6:7 title TITLE1."(Soft Delay)", \
	 FILE1 using 8:9 title TITLE1."(Non Real-Time)", \
	 FILE2 using 2:3 title TITLE2."(Hard Real-Time)", \
	 FILE2 using 4:5 title TITLE2."(Soft Bandwidth)", \
	 FILE2 using 6:7 title TITLE2."(Soft Delay)", \
	 FILE2 using 8:9 title TITLE2."(Non Real-Time)"#, \
	 #FILE3 using 2:3 title TITLE3."(Hard Real-Time)", \
	 #FILE3 using 4:5 title TITLE3."(Soft Bandwidth)", \
	 #FILE3 using 6:7 title TITLE3."(Soft Delay)", \
	 #FILE3 using 8:9 title TITLE3."(Non Real-Time)"

#Disaster Requests(COS)
set title  TITLEBASE."\nDP (COS)"
set ylabel "DP (%)"
set output OUTPATH."DP_COS.png"
plot FILE1 using 10:11:xticlabels(1) title TITLE1."(Hard Real-Time)", \
	 FILE1 using 12:13 title TITLE1."(Soft Bandwidth)", \
	 FILE1 using 14:15 title TITLE1."(Soft Delay)", \
	 FILE1 using 16:17 title TITLE1."(Non Real-Time)", \
	 FILE2 using 10:11 title TITLE2."(Hard Real-Time)", \
	 FILE2 using 12:13 title TITLE2."(Soft Bandwidth)", \
	 FILE2 using 14:15 title TITLE2."(Soft Delay)", \
	 FILE2 using 16:17 title TITLE2."(Non Real-Time)"#, \
	 #FILE3 using 10:11 title TITLE3."(Hard Real-Time)", \
	 #FILE3 using 12:13 title TITLE3."(Soft Bandwidth)", \
	 #FILE3 using 14:15 title TITLE3."(Soft Delay)", \
	 #FILE3 using 16:17 title TITLE3."(Non Real-Time)"

#Cost
set title  TITLEBASE."\nCosts"
set ylabel "Cost ($)"
set output OUTPATH."Cost.png"
plot FILE1 using 18:19:xticlabels(1) title TITLE1."(Blocked Cost)", \
	 FILE1 using 20:21 title TITLE1."(Disaster Cost)", \
	 FILE1 using ($18+$20):(sqrt((($19)**2)+(($21)**2))) title TITLE1."(TOTAL)", \
	 FILE2 using 18:19 title TITLE2."(Blocked Cost)", \
	 FILE2 using 20:21 title TITLE2."(Disaster Cost)", \
	 FILE2 using ($18+$20):(sqrt((($19)**2)+(($21)**2))) title TITLE2."(TOTAL)"#, \
     #FILE3 using 18:19 title TITLE3."(Blocked Cost)", \
	 #FILE3 using 20:21 title TITLE3."(Disaster Cost)", \
	 #FILE3 using ($18+$20):(sqrt((($19)**2)+(($21)**2))) title TITLE3."(TOTAL)"

#Blocked Requests
set title  TITLEBASE."\nBlocked Requests"
set ylabel "Blocked (%)"
set output OUTPATH."Blocked_COS.png"
plot FILE1 using 26:27:xticlabels(1) title TITLE1." By DC", \
     FILE1 using 28:29 title TITLE1." By VNF" , \
	 FILE1 using 30:31 title TITLE1." By BW"  , \
	 FILE1 using 32:33 title TITLE1." By Path", \
	 FILE2 using 26:27 title TITLE2." By DC", \
     FILE2 using 28:29 title TITLE2." By VNF" , \
	 FILE2 using 30:31 title TITLE2." By BW"  , \
	 FILE2 using 32:33 title TITLE2." By Path"#, \
	 #FILE3 using 26:27 title TITLE3." By DC", \
     #FILE3 using 28:29 title TITLE3." By VNF" , \
	 #FILE3 using 30:31 title TITLE3." By BW"  , \
	 #FILE3 using 32:33 title TITLE3." By Path"
exit

