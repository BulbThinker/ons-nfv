package nfvs;

import nfvs.utils.RandomE;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class VNF extends VNFData{

	public  static List<Integer> all_functions;
	public  static RandomE rand;
	
	private int active_requests;
	private Datacenter datacenter;
	
    static {
		VNF.all_functions = new ArrayList<Integer>();
		rand = new RandomE(1,0);
    }

    public VNF(VNFData data, Datacenter datacenter) {
        super(data);
        this.active_requests = 0;
        this.datacenter = datacenter;
    }

    public boolean newRequest() {
        if(this.capacity > 0) {
            this.active_requests++;
            this.capacity--;
        }
        return false;
    }

    public void endRequest() {
        if(this.active_requests > 0) {
            this.active_requests--;
            this.capacity++;
        }
    }
    
    public boolean hasCapacity() {
    	return (this.capacity > 0);
    }
    
    public int getActiveRequests() {
        return this.active_requests;
    }
    
    public Datacenter getDatacenter() {
    	return this.datacenter;
    }
	
	public static List<Integer> getRandomFunctions() {
		List<Integer> list = new LinkedList<Integer>();
		int size = VNF.all_functions.size();
		int function;
		//Collections.shuffle(list);
		while(list.size() < 4) {
			function = VNF.all_functions.get(rand.nextInt(size));
			if(!list.contains(function)){
				list.add(function);
			}
		}
		return list;
	}
}