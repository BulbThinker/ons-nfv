package nfvs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ons.OXC;

public class Datacenter extends OXC{
	
	private Map<Integer,Integer>  	resources;
	private Map<Integer,VNFData>  	available_functions;
	private List<VNF>		  		instantiated_functions;
	
	public Datacenter(int id, int groomingInputPorts, int groomingOutputPorts) {
		super(id, groomingInputPorts, groomingOutputPorts);
		this.resources = new HashMap<Integer, Integer>();
		this.available_functions = new HashMap<Integer, VNFData>();
		this.instantiated_functions = new ArrayList<VNF>();
	}
	
	public void setResource(int resource, int value) {
		this.resources.put(resource, value);
	}
	
	/**
	 *
	 */
	public VNFData setAvailableFunction(int fid, int setup_cost, int capacity) {
		VNFData vnfd = null;
		if (fid < 0 || setup_cost < 0 || capacity < 0) {
			throw (new IllegalArgumentException());
		}
		else {
			vnfd =  new VNFData(fid, setup_cost, capacity);
			this.available_functions.put(fid, vnfd);
		}
		return vnfd;
	}

	/**
	 *
	 */
	public synchronized void tryFreeResources() {
		Iterator<VNF> it = this.instantiated_functions.iterator();
		while(it.hasNext()) {
			VNF vnf = it.next();
			if (vnf.getActiveRequests() <= 0) {
				int value;
				for(int res : vnf.getResourcesTypes()) {
					value = this.resources.get(res) + vnf.getResourceCost(res);
					this.resources.put(res, value);
				}
				it.remove();
			}
		}
	}
	
	public synchronized boolean tryFreeResources(VNF vnf) {
		if(vnf.getDatacenter() == this) {
			if (vnf.getActiveRequests() <= 0) {
				int value;
				for(int res : vnf.getResourcesTypes()) {
					value = this.resources.get(res) + vnf.getResourceCost(res);
					this.resources.put(res, value);
				}
				return true;
			}
		}
		return false;
	}
	
	public synchronized VNF hasInstantiatedFunctionWithCapacity(int fid) {
		for(VNF f : this.instantiated_functions) {
			if((fid == f.getID()) && f.hasCapacity()) {
				return f;
			}
		}
		return null;
	}

	public synchronized VNF instantiateFunction(int function) {
		VNFData vnfd = this.available_functions.get(function);
		if(vnfd != null && this.hasResourcesTo(vnfd)) {
			VNF vnf = new VNF(vnfd, this);
			int value;
			for(int res : vnfd.getResourcesTypes()) {
				value = this.resources.get(res) - vnfd.getResourceCost(res);
				this.resources.put(res, value);
			}
			this.instantiated_functions.add(vnf);
			return vnf;
		}
		return null;
	}

	public int getAvailableResources(int resource) {
		return this.resources.get(resource);
	}
	
	public boolean hasResourcesTo(int vnf) {
		VNFData function = this.available_functions.get(vnf);
		return hasResourcesTo(function);
	}
	
	//TODO: Remove Instantiated Check
	public boolean hasResourcesTo(VNFData vnf) {
		if(vnf == null) {
			return false;
		}

		int[] required_res = vnf.getResourcesTypes();
		for(int res : required_res) {
			if (this.resources.get(res) < vnf.getResourceCost(res)) {
				return false;
			}
		}
		return true;
	}

	
	public int implementAtCost(int fid) {
		VNFData vnf = this.available_functions.get(fid);
		if(vnf != null) {
			return vnf.getSetupCost();
		}
		return -1;
	}

}
