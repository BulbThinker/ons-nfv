package nfvs.utils.Dijkstra;

import ons.EONLink;
import ons.EONPhysicalTopology;
import ons.Link;
import ons.WDMLink;
import ons.util.WeightedGraph;

public class DijkstraMB extends DijkstraExtra{
	@Override
	public  double getDijkstraWeight(WeightedGraph G, Link link, int r) {
		if(link instanceof WDMLink) {
        	double abw = 0;
        	for(int wl = 0; wl < ((WDMLink)link).getWavelengths(); ++wl) {
        		double aux = ((WDMLink)link).amountBWAvailable(wl);
        		if(aux > abw) {
        			abw = aux;
        		}
        	}
        	if(r > abw) {
        		return Double.POSITIVE_INFINITY;
        	}
        	else {
        		return ((double)r/abw);
        	}
        }
        else {
        	//EON
        	if(r > (((EONLink)link).getAvaiableSlots() * EONPhysicalTopology.getSlotSize())) {
        		return Double.POSITIVE_INFINITY;
        	}
        	else {
        		return ((double)r/(((EONLink)link).getAvaiableSlots() * EONPhysicalTopology.getSlotSize()));
        	}
        }
	}
}
