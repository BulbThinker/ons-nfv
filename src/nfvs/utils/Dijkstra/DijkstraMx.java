package nfvs.utils.Dijkstra;

import nfvs.WDMLinkE;
import ons.EONLink;
import ons.EONPhysicalTopology;
import ons.Link;
import ons.WDMLink;
import ons.util.WeightedGraph;

public class DijkstraMx extends DijkstraExtra{
	
	@Override
	public  double getDijkstraWeight(WeightedGraph G, Link link, int r) {
		if(link instanceof WDMLinkE) {
        	double abw = 0;
        	for(int wl = 0; wl < ((WDMLinkE)link).getWavelengths(); ++wl) {
        		double aux = ((WDMLinkE)link).amountBWAvailable(wl);
        		if(aux > abw) {
        			abw = aux;
        		}
        	}
        	if(r > abw) {
        		return Double.POSITIVE_INFINITY;
        	}
        	else {
        		return (((WDMLinkE)link).getSetupCost() + ((double)link.getWeight()/100.0) + ((double)r/abw));
        	}
        }
        else {
        	//EON
        	if(r > (((EONLink)link).getAvaiableSlots() * EONPhysicalTopology.getSlotSize())) {
        		return Double.POSITIVE_INFINITY;
        	}
        	else {
        		return (((WDMLinkE)link).getSetupCost() +((double)link.getWeight()/100.0) + ((double)r/(((EONLink)link).getAvaiableSlots() * EONPhysicalTopology.getSlotSize())));
        	}
        }
	}
	
}
