package nfvs.events;

import ons.Link;

public class DisasterPredictionEvent extends EventE {
    private DisasterEvent devent;

    public DisasterPredictionEvent() {
        devent = null;
    }

    public Link[] execute() {
        Link[] aux = this.devent.defineEvent();
        eventScheduler.addEvent(this.devent);
        return aux;
    }

    public void setEvent(DisasterEvent event) {
        devent = event;
    }

    public DisasterEvent getEvent() {
        return devent;
    }

}
