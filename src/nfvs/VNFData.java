package nfvs;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class VNFData {
    protected int id;
    protected int setup_cost;
    protected int capacity;
    protected Map<Integer, Integer> resources_cost;
    
    
    public VNFData(int id, int setup_cost, int capacity) {
        if (id < 0 || setup_cost < 0 || capacity < 0) {
            throw (new IllegalArgumentException());
        }
        else {
            this.id = id;
            this.setup_cost = setup_cost;
            this.capacity = capacity;
            this.resources_cost = new HashMap<Integer, Integer>();
        }
    }
    
    public VNFData(VNFData data) {
    	this.id             = data.id;
    	this.setup_cost     = data.setup_cost;
    	this.capacity       = data.capacity;
    	this.resources_cost = data.resources_cost ;
    }
    
    public void setSetupCost(int setup_cost) {
        if (setup_cost < 0 ) {
            throw (new IllegalArgumentException());
        }
        else {
            this.setup_cost = setup_cost;
        }
    }
    
    public void setCapacity(int capacity) {
        if (capacity < 0) {
            throw (new IllegalArgumentException());
        }
        else {
        	this.capacity = capacity;
        }
    }
    
    public void setResourceCost(int resource, int cost) {
    	this.resources_cost.put(resource, cost);
    }
    
    public void setResourceCost(int resource, int cost_min, int cost_max) {
    	this.resources_cost.put(resource, NFVRandom.nextIntInterval(cost_min, cost_max));
    }

    public int getID() {
        return this.id;
    }

    public int getSetupCost() {
        return this.setup_cost;
    }
    
    public int getResourceCost(int resource) {
    	Integer i = this.resources_cost.get(resource);
    	if (i == null)
    		return -1;
    	return i;
    }
    
    public int[] getResourcesTypes() {
    	Set<Integer> key_set = this.resources_cost.keySet();
    	Iterator<Integer> it = key_set.iterator();
    	int[] keys = new int[key_set.size()];
    	for(int i = 0; i < keys.length; i++) {
    		keys[i] = it.next();
    	}
    	return keys;
    }
    
    public int getCapacity() {
        return this.capacity;
    }
}