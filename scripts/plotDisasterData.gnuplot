#!/usr/bin/gnuplot
#Constants
OUTPATH="../GRAPH/MyNFVRWA_RA/"
DATAFILE='../DATA/MyNFVRWA_RA_ERLANGS/RESULT.data'
TITLEBASE="MyNFVRWA CSP"

#Initial Configuration
set term png size 1024,768
set xlabel "Load (Erlangs)"

set boxwidth 0.9 relative
set style data histogram
set style fill solid 1.0 border -1
set bars fullwidth
set style histogram errorbars gap 2 lw 2

#Blocked Requests
set ylabel "Blocked Requests"
set title  TITLEBASE."\nBlocked Requests COS"
set output OUTPATH."Blocked.png"
plot DATAFILE using 2:3:xticlabels(1) title "Hard", \
           "" using 4:5 title "Soft Band", \
		   "" using 6:7 title "Soft Delay", \
		   "" using 8:9 title "Non"  

#Interrupted Requests
set ylabel "Interrupted Requests"
set title  TITLEBASE."\nInterrupted Requests COS"
set output OUTPATH."Interrupted.png"
plot DATAFILE using 10:11:xticlabels(1) title "Hard" , \
           "" using 12:13 title "Soft Band", \
		   "" using 14:15 title "Soft Delay", \
		   "" using 16:17 title "Non"
		   
#Blocked Cost
set ylabel "Cost ($)"
set title  TITLEBASE."\nBlocked Costs"
set output OUTPATH."BlockedCost.png"
plot DATAFILE using 18:19:xticlabels(1) title "Blocked Cost"

#Disaster Cost
set title  TITLEBASE."\nDisaster Costs"
set output OUTPATH."DisasterCost.png"
plot DATAFILE using 20:21:xticlabels(1) title "Disaster Cost"

exit
