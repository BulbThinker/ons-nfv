package nfvs.rra;

import nfvs.Request;
import ons.ra.ControlPlaneForRA;

import java.util.Collection;

public interface RRA {
    void simulationInterface(ControlPlaneForRA cp);
    Request[][] recovery(Collection<Request> requests);
    boolean flowRecovery(Request request);
}
