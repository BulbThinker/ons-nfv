package nfvs.disaster;

import nfvs.DisasterPredictor;
import nfvs.events.DisasterEvent;
import nfvs.events.DisasterPredictionEvent;
import nfvs.events.RepairEvent;
import nfvs.utils.RandomE;
import ons.*;

public class DisasterGenerator {
    public  static double MTBF;
    public  static double MRT;
    public  static int selected;
    public  static int disastersNumber;
    public  static RandomE rand;

    static {
        MTBF = 10;
        MRT  = 10;
        disastersNumber = 0;
        rand = new RandomE(1,0);
    }

    public static void generate(PhysicalTopology pt, EventScheduler events) {
        double lastTimeD = 0;
        for(int i = 0; i < disastersNumber; ++i) {
            DisasterZone zone = (selected<0)?DisasterZone.getRandomZone():DisasterZone.getZone(selected);
            if (zone == null)
                continue;
            //Disaster
            DisasterEvent Devent = new DisasterEvent();
            Devent.setTime(lastTimeD += rand.nextExponentialB(MTBF));
            Devent.setZone(zone);

            events.addEvent(DisasterPredictor.predict(Devent));

            if(Simulator.verbose) {
                System.out.println("        Zone " + zone.getId() + ", Time " + lastTimeD);
            }
        }
    }

}
